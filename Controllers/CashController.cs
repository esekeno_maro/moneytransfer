﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MoneyTransfer.Models;
using MoneyTransfer.Repositories;
using MoneyTransfer.ViewModels;

namespace MoneyTransfer.Controllers
{
   
        public class CashController : Controller
        {
            public ApplicationContext context;
            //private readonly UserManager<User> _userManager;
            private readonly ICashRepository cashRepository;
            

            public CashController(ApplicationContext context, ICashRepository cashRepository)
            {
                this.context = context;
                this.cashRepository = cashRepository;
                

            }
            [HttpPost]
            public async Task<IActionResult> SendMoneyTo(CashViewModel us)
            {

                User user = context.Users.FirstOrDefault(u => u.PersonalNumber == us.PersonalNumber);
                User thisUser = context.Users.First(u => u.UserName == User.Identity.Name);

                if (thisUser.Balance > us.Balance)
                {
                    user.Balance += us.Balance;
                    thisUser.Balance -= us.Balance;
                    Cash transfer = new Cash()
                    {
                        TransferDate = DateTime.Now,
                        Sum = us.Balance,
                        FromId = thisUser.Id,
                        ToId = user.Id
                    };
                    await cashRepository.AddEntity(transfer);
                }


                return RedirectToAction("Index", "Home");
            }

            [HttpGet]
            public IActionResult TransferToAnyAccount()
            {
                return View();
            }
            [HttpPost]
            public IActionResult TransferToAnyAccount(CashViewModel us)
            {
                User user = context.Users.FirstOrDefault(u => u.PersonalNumber == us.PersonalNumber);

                if (user != null)
                {
                    user.Balance += us.Balance;
                    context.SaveChanges();
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Personal number not exist");
                    return View();
                }
                return RedirectToAction("Index", "Home");
            }

            public IActionResult GetListOfTransfers(User userName)
            {
                User user = context.Users.First(u => u.UserName == User.Identity.Name);
                

                //List<Cash> transfers = context.Transfers.Include(u => u.FromUser).Include(u=>u.ToUser).Where
                //    (t => t.FromId == user.Id || t.ToId == user.Id).ToList();
                List<Cash> transfers = cashRepository.GetTransfersByUserId(user.Id);
                return View(transfers);

            }
        }
    
}