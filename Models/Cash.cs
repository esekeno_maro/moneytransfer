﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.Models
{
        public class Cash : Entity
        {
            public DateTime TransferDate { get; set; }
            public int Sum { get; set; }

            [ForeignKey("FromUser")]
            public string FromId { get; set; }
            [ForeignKey("ToUser")]
            public string ToId { get; set; }

            public User ToUser { get; set; }
            public User FromUser { get; set; }
        }
    
}
