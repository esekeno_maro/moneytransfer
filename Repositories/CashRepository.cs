﻿using Microsoft.EntityFrameworkCore;
using MoneyTransfer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.Repositories
{
    public interface ICashRepository : IRepository<Cash>
    {
        List<Cash> GetTransfersByUserId(string id);
    }

    public class CashRepository : Repository<Cash>, ICashRepository
    {
        public CashRepository(ApplicationContext context)
            : base(context)
        {
            AllEntities = context.Transfers;
        }

        public List<Cash> GetTransfersByUserId(string id)
        {
            return AllEntities.Include(u => u.FromUser).Include(u => u.ToUser)
                .Where(t => t.FromId == id || t.ToId == id).ToList();
        }
    }
}
