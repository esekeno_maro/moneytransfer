﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.ViewModels
{
    public class CashViewModel
    {
        [Required]
        public string PersonalNumber { get; set; }

        public int Balance { get; set; }
    }
}
